<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/test', function () {
    return 1;
});

Route::get('/test2', function () {
    return 22;
});

Route::get('test3', function () {
    return 3333;
});

Route::get('test5', function () {
    return 333333;
});


Route::get('/test6', function () {
    return 111;
});

Route::get('/test13', function () {
    return 123123123;
});

Route::get('/test14', function () {
    return 12312312312;
});

Route::get('/test15', function () {
    return 1231231231231;
});

Route::get('test16', function () {
    return 1233;
});

Route::get('/test17', function () {
    return 1231423;
});

Route::get('/test17', function () {
    return 1444;
});

Route::get('/test18', function () {
    return 1833e444;
});

Route::get('/test181', function () {
    echo 1223;
});

Route::get('/test20', function () {
    echo "2022222s2";
});
